﻿using System.Globalization;
using CsvHelper;
using ReportService;
using Services;

namespace PowerPositionService
{
    public class PowerPositionProcessor
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private readonly IPowerService _powerService;

        public PowerPositionProcessor(ILogger<PowerPositionProcessor> logger, IConfiguration config, IPowerService powerService)
        {
            _logger = logger;
            _config = config;
            _powerService = powerService;
        }

        internal async Task ExtractPdf()
        {

            try
            {
                var date = DateTime.Now;

                _logger.LogInformation($"Pdf extract started at: {date:dd.MM.yyyy HH:mm}");

                var britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
                var localTime = TimeZoneInfo.ConvertTime(date, TimeZoneInfo.Local, britishZone);
                var londonEffectiveDate = GetEffectiveDate(localTime);

                var tasks = await _powerService.GetTradesAsync(londonEffectiveDate);

                var allPeriods = tasks.SelectMany(x => x.Periods.ToList());
                var periods = allPeriods
                    .GroupBy(period => period.Period)
                    .Select(x => new PowerPeriodDto
                    {
                        LocalTime = DateTime.Today.AddHours(x.Key - 2),
                        Volume = x.Sum(p => p.Volume)
                    }).ToList();

                await using var writer = new StreamWriter($"{_config["FileLocation"]}\\PowerPosition_{localTime:yyyyMMdd}_{localTime:HHmm}.csv");
                await using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);
            
                await csv.WriteRecordsAsync(periods);

                _logger.LogInformation($"file: PowerPosition_{localTime:yyyyMMdd}_{localTime:HHmm}.csv successfully extracted");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.LogCritical(e.Message, e);
            }
        }

        private DateTime GetEffectiveDate(DateTime londonTime)
        {
            return londonTime.Hour >= 23 ? londonTime.Date.AddDays(1) : londonTime.Date;
        }
    }
}
