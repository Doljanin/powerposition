using PowerPositionService;
using Serilog;
using Services;

IHost host = Host.CreateDefaultBuilder(args)
    .UseSerilog((hostingContext, loggerConfig) =>
        loggerConfig.ReadFrom
            .Configuration(hostingContext.Configuration)
    )
    .ConfigureServices(services =>
    {
        services.AddTransient<PowerPositionProcessor>();
        services.AddTransient<IPowerService, PowerService>();
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
