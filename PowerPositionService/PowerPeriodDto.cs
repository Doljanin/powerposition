﻿using CsvHelper.Configuration.Attributes;

namespace ReportService;

class PowerPeriodDto
{
    [Name("Local Time")]
    [Format("HH:mm")]
    public DateTime LocalTime { get; set; }

    [Name("Volume")]
    public double Volume { get; set; }
}