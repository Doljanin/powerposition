namespace PowerPositionService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IServiceScopeFactory _scope;
        private readonly IConfiguration _config;

        public Worker(ILogger<Worker> logger, IServiceScopeFactory scope, IConfiguration config)
        {
            _logger = logger;
            _scope = scope;
            _config = config;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                if (!int.TryParse(_config["ExtractIntervalInMinutes"], out var intervalInMinutes))
                    throw new Exception("Minutes interval is not set correctly");
                if(string.IsNullOrEmpty(_config["FileLocation"]))
                    throw new Exception("File path is not set!");

                var timer = new PeriodicTimer(TimeSpan.FromMinutes(intervalInMinutes));

                do
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                    var service = _scope.CreateScope().ServiceProvider.GetRequiredService<PowerPositionProcessor>();

                    service.ExtractPdf();

                } while (await timer.WaitForNextTickAsync(stoppingToken));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.LogCritical(e.Message, e);
                Dispose();
            }
        }
    }
}